<?xml version="1.0" standalone="yes" ?>
<script>
    <property key="dff.date" value="Wed Jan 27 09:45:36 2016" />
    <property key="dff.version" value="2.4.1.20140602" />
    <property key="metadata.author" value="Designed and coded by Adrian Ghilardi (CIGA-UNAM), with contributions from Jean-François Mas (CIGA-UNAM), Robert Bailis (SEI), Rudi Drigo (Independent consultant) and Omar Masera (IIES-UNAM)." />
    <property key="metadata.description" value='&quot;Mofuss&quot; is a dynamic model that simulates the effects of fuelwood harvesting on vegetation, accounting for savings in non-renewable woody biomass from reduced consumption due to an external intervention, such as an improved cookstove (ICS) or fuel substitution project.' />
    <property key="metadata.notes" value='&quot;Mofuss&quot; (version 1.0) was developed between September 2011 and April 2015 with funding from Yale Institute for Biospheric Studies, Global Alliance for Clean Cookstoves, Overlook International Foundation, ClimateWorks Foundation and UNAM’s PAPIIT IA101513.' />
    <property key="metadata.organization" value="UNAM - Yale - SEI" />
    <property key="metadata.showproperties" value="no" />
    <property key="metadata.title" value="Crop_locs" />
    <property key="metadata.version" value="1.0" />
    <property key="metadata.wizard" value='{&#x0A;&quot;inputPages&quot;: [&#x0A;{&#x0A;&quot;title&quot;: &quot;Selecting localities&quot;,&#x0A;&quot;content&quot;: &quot;&lt;html&gt;&#x0A;&lt;head/&gt;&#x0A;&lt;body bgcolor=&apos;&apos;&apos;white&apos;&apos;&apos;&gt;&#x0A;&lt;p style=&apos;&apos;&apos;color:gray;font-size:20px;font-family:verdana;&apos;&apos;&apos;&gt;&#x0A;&lt;span style=&apos;&apos;&apos;font-family:Verdana; font-size:12pt; color:#0000cc;&apos;&apos;&apos;&gt;&#x0A;&lt;b&gt;Step 3: Clipping selected localities and calculating their proportion of fuelwood use:&lt;/b&gt;&#x0A;&lt;/span&gt;&#x0A;&lt;span style=&apos;&apos;&apos;font-family:Verdana; font-size:12pt;&apos;&apos;&apos;&gt;This will clip selected localities (if any) an account for the proportion of fuelwood from deforestation that should be allocated to those localities.&lt;/span&gt;&#x0A;&lt;/p&gt;&#x0A;&lt;p style=&apos;&apos;&apos;font-family:verdana;font-size:20px;color:gray;&apos;&apos;&apos;&gt;&#x0A;&lt;img src=&apos;&apos;&apos;LULCC/Wizard_imgs/SelectedLocs.jpeg&apos;&apos;&apos; border=&apos;&apos;&apos;0&apos;&apos;&apos;/&gt;&#x0A;&lt;/p&gt;&#x0A;&lt;p style=&apos;&apos;&apos;font-family:verdana;font-size:20px;color:gray;&apos;&apos;&apos;&gt;   &lt;/p&gt;&#x0A;&lt;/body&gt;&#x0A;&lt;/html&gt;&#x0A;&quot;,&#x0A;&quot;editors&quot;: [&#x0A;{&#x0A;&quot;name&quot;: &quot;Localities of interest&quot;,&#x0A;&quot;description&quot;: &quot;Did you save a sample of selected localities that you want to analyze in isolation from all villages and cities using fuelwood within the study area? (i.e. did you save an \&quot;Extent Locs.shp\&quot; file?)&quot;,&#x0A;&quot;tag&quot;: &quot;Bool_constant_1&quot;&#x0A;}&#x0A;]&#x0A;}&#x0A;],&#x0A;&quot;outputPages&quot;: [&#x0A;{&#x0A;&quot;title&quot;: &quot;Default&quot;,&#x0A;&quot;content&quot;: &quot;&lt;html&gt;&#x0A;&lt;head/&gt;&#x0A;&lt;body bgcolor=&apos;&apos;&apos;white&apos;&apos;&apos;&gt;&#x0A;&lt;p&gt; &lt;span style=&apos;&apos;&apos;font-size:12pt; color:#0000cc; font-family:Verdana;&apos;&apos;&apos;&gt;&#x0A;&lt;b&gt;That was &lt;/b&gt;&#x0A;&lt;/span&gt;&#x0A;&lt;b&gt; &lt;span&gt;&#x0A;&lt;img border=&apos;&apos;&apos;0&apos;&apos;&apos; width=&apos;&apos;&apos;126&apos;&apos;&apos; height=&apos;&apos;&apos;49&apos;&apos;&apos; style=&apos;&apos;&apos;font-size:12pt;color:#0000cc;font-family:Verdana;font-weight:bold;&apos;&apos;&apos; src=&apos;&apos;&apos;LULCC/Wizard_imgs/fastlogo.jpg&apos;&apos;&apos;/&gt;&#x0A;&lt;/span&gt;&#x0A;&lt;/b&gt;&#x0A;&lt;/p&gt;&#x0A;&lt;p&gt; &lt;span style=&apos;&apos;&apos;font-size:12pt; color:#0000cc; font-family:Verdana;&apos;&apos;&apos;&gt;L&lt;/span&gt;&#x0A;&lt;span style=&apos;&apos;&apos;font-size:12pt; font-family:Verdana;&apos;&apos;&apos;&gt;et&apos;s now hit Step 4: Building friction maps, or impedance to displacement.&lt;/span&gt;&#x0A;&lt;/p&gt;&#x0A;&lt;/body&gt;&#x0A;&lt;/html&gt;&#x0A;&quot;,&#x0A;&quot;editors&quot;: []&#x0A;}&#x0A;]&#x0A;}' />
    <containerfunctor name="Group">
        <property key="dff.functor.alias" value="group1476" />
        <functor name="SaveLookupTable">
            <property key="dff.functor.alias" value="saveLookupTable3355" />
            <inputport name="table" peerid="v7" />
            <inputport name="filename">&quot;In/fwuse_V_ext_fwdef.csv&quot;</inputport>
            <inputport name="suffixDigits">2</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
        </functor>
        <functor name="SaveLookupTable">
            <property key="dff.functor.alias" value="saveLookupTable3409" />
            <inputport name="table" peerid="v11" />
            <inputport name="filename">&quot;In/fwuse_V_clipped.csv&quot;</inputport>
            <inputport name="suffixDigits">2</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
        </functor>
        <functor name="SaveLookupTable">
            <property key="dff.functor.alias" value="saveLookupTable3411" />
            <inputport name="table" peerid="v12" />
            <inputport name="filename">&quot;In/fwuse_W_clipped.csv&quot;</inputport>
            <inputport name="suffixDigits">2</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
        </functor>
        <functor name="CategoricalMapJunction">
            <property key="dff.functor.alias" value="categoricalMapJunction1143" />
            <inputport name="possibleMap1" peerid="v17" />
            <inputport name="possibleMap2" peerid="v18" />
            <outputport name="map" id="v1" />
        </functor>
        <functor name="ExtractMapValues">
            <property key="dff.functor.alias" value="extractMapValues1160" />
            <property key="viewer.cells" value="yes" />
            <inputport name="map" peerid="v10" />
            <inputport name="useMapValuesAsKeys">.yes</inputport>
            <outputport name="cells" id="v2" />
        </functor>
        <functor name="GetTableColumn">
            <property key="dff.functor.alias" value="getTableColumn1164" />
            <property key="viewer.result" value="yes" />
            <inputport name="table" peerid="v2" />
            <inputport name="columnIndexOrName">2</inputport>
            <outputport name="result" id="v3" />
        </functor>
        <functor name="CalcLookupTableIntersection">
            <property key="dff.functor.alias" value="calcLookupTableIntersection1168" />
            <property key="viewer.result" value="yes" />
            <inputport name="firstTable" peerid="v15" />
            <inputport name="secondTable" peerid="v3" />
            <inputport name="useFirstTableToSolveDuplicatedKey">.yes</inputport>
            <outputport name="result" id="v4" />
        </functor>
        <functor name="ExtractMapValues">
            <property key="dff.functor.alias" value="extractMapValues1173" />
            <inputport name="map" peerid="v10" />
            <inputport name="useMapValuesAsKeys">.yes</inputport>
            <outputport name="cells" id="v5" />
        </functor>
        <functor name="GetTableColumn">
            <property key="dff.functor.alias" value="getTableColumn1175" />
            <inputport name="table" peerid="v5" />
            <inputport name="columnIndexOrName">2</inputport>
            <outputport name="result" id="v6" />
        </functor>
        <functor name="CalcLookupTableIntersection">
            <property key="dff.functor.alias" value="calcLookupTableIntersection1177" />
            <property key="viewer.result" value="yes" />
            <inputport name="firstTable" peerid="v16" />
            <inputport name="secondTable" peerid="v6" />
            <inputport name="useFirstTableToSolveDuplicatedKey">.yes</inputport>
            <outputport name="result" id="v7" />
        </functor>
        <functor name="ExtractMapValues">
            <property key="dff.functor.alias" value="extractMapValues1185" />
            <inputport name="map" peerid="v1" />
            <inputport name="useMapValuesAsKeys">.yes</inputport>
            <outputport name="cells" id="v8" />
        </functor>
        <functor name="ExtractMapValues">
            <property key="dff.functor.alias" value="extractMapValues1187" />
            <inputport name="map" peerid="v1" />
            <inputport name="useMapValuesAsKeys">.yes</inputport>
            <outputport name="cells" id="v9" />
        </functor>
        <functor name="LoadCategoricalMap">
            <property key="dff.functor.alias" value="loadCategoricalMap1141" />
            <inputport name="filename">&quot;LULCC/TempRaster/Locs_c.tif&quot;</inputport>
            <inputport name="nullValue">.none</inputport>
            <inputport name="loadAsSparse">.no</inputport>
            <inputport name="suffixDigits">0</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
            <outputport name="map" id="v10" />
        </functor>
        <functor name="CalcLookupTableIntersection">
            <property key="dff.functor.alias" value="calcLookupTableIntersection1145" />
            <property key="viewer.result" value="yes" />
            <inputport name="firstTable" peerid="v16" />
            <inputport name="secondTable" peerid="v13" />
            <inputport name="useFirstTableToSolveDuplicatedKey">.yes</inputport>
            <outputport name="result" id="v11" />
        </functor>
        <functor name="CalcLookupTableIntersection">
            <property key="dff.functor.alias" value="calcLookupTableIntersection1147" />
            <property key="viewer.result" value="yes" />
            <inputport name="firstTable" peerid="v15" />
            <inputport name="secondTable" peerid="v14" />
            <inputport name="useFirstTableToSolveDuplicatedKey">.yes</inputport>
            <outputport name="result" id="v12" />
        </functor>
        <functor name="GetTableColumn">
            <property key="dff.functor.alias" value="getTableColumn1151" />
            <inputport name="table" peerid="v8" />
            <inputport name="columnIndexOrName">2</inputport>
            <outputport name="result" id="v13" />
        </functor>
        <functor name="GetTableColumn">
            <property key="dff.functor.alias" value="getTableColumn1153" />
            <inputport name="table" peerid="v9" />
            <inputport name="columnIndexOrName">2</inputport>
            <outputport name="result" id="v14" />
        </functor>
        <functor name="Bool">
            <property key="dff.functor.alias" value="bool1132" />
            <inputport name="constant" peerid="v20" />
        </functor>
        <functor name="SaveLookupTable">
            <property key="dff.functor.alias" value="saveLookupTable1147" />
            <inputport name="table" peerid="v4" />
            <inputport name="filename">&quot;In/fwuse_W_ext_fwdef.csv&quot;</inputport>
            <inputport name="suffixDigits">2</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
        </functor>
        <functor name="LoadLookupTable">
            <property key="dff.functor.alias" value="loadLookupTable1150" />
            <inputport name="filename">&quot;LULCC/TempTables/fwuse_W.csv&quot;</inputport>
            <inputport name="suffixDigits">0</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
            <outputport name="table" id="v15" />
        </functor>
        <functor name="LoadLookupTable">
            <property key="dff.functor.alias" value="loadLookupTable1154" />
            <inputport name="filename">&quot;LULCC/TempTables/fwuse_V.csv&quot;</inputport>
            <inputport name="suffixDigits">0</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
            <outputport name="table" id="v16" />
        </functor>
        <containerfunctor name="IfThen">
            <property key="dff.functor.alias" value="ifThen1135" />
            <inputport name="condition" peerid="v19" />
            <functor name="LoadCategoricalMap">
                <property key="dff.functor.alias" value="loadCategoricalMap1141" />
                <inputport name="filename">&quot;LULCC/TempRaster/Ext_Locs_c.tif&quot;</inputport>
                <inputport name="nullValue">.none</inputport>
                <inputport name="loadAsSparse">.no</inputport>
                <inputport name="suffixDigits">0</inputport>
                <inputport name="step">.none</inputport>
                <inputport name="workdir">.none</inputport>
                <outputport name="map" id="v17" />
            </functor>
        </containerfunctor>
        <containerfunctor name="IfNotThen">
            <property key="dff.functor.alias" value="ifNotThen1137" />
            <inputport name="condition" peerid="v19" />
            <functor name="LoadCategoricalMap">
                <property key="dff.functor.alias" value="loadCategoricalMap1143" />
                <inputport name="filename">&quot;LULCC/TempRaster/Locs_c.tif&quot;</inputport>
                <inputport name="nullValue">.none</inputport>
                <inputport name="loadAsSparse">.no</inputport>
                <inputport name="suffixDigits">0</inputport>
                <inputport name="step">.none</inputport>
                <inputport name="workdir">.none</inputport>
                <outputport name="map" id="v18" />
            </functor>
        </containerfunctor>
    </containerfunctor>
    <containerfunctor name="Group">
        <property key="dff.functor.alias" value="group1488" />
        <functor name="RunExternalProcess">
            <property key="dff.functor.alias" value="runExternalProcess1490" />
            <inputport name="fileName" peerid="v22" />
            <inputport name="parameters" peerid="v23" />
            <inputport name="waitProcessCompletion">.yes</inputport>
            <inputport name="secondsToWait">0</inputport>
        </functor>
        <functor name="Bool">
            <property key="dff.functor.alias" value="Did you save a sample of selected localities within the analysis area? (Extent_Locs.shp)" />
            <property key="wizard.constant.input" value="Bool_constant_1" />
            <inputport name="constant">.no</inputport>
            <outputport name="object" id="v19" />
        </functor>
        <functor name="Bool">
            <property key="dff.functor.alias" value="bool1130" />
            <inputport name="constant">.yes</inputport>
            <outputport name="object" id="v20" />
        </functor>
        <functor name="LoadTable">
            <property key="dff.functor.alias" value="loadTable3334" />
            <inputport name="filename">&quot;LULCC/TempTables/Rpath.csv&quot;</inputport>
            <inputport name="suffixDigits">0</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
            <outputport name="table" id="v21" />
        </functor>
        <functor name="GetTableValue">
            <property key="dff.functor.alias" value="getTableValue3336" />
            <inputport name="table" peerid="v21" />
            <inputport name="keys">[ 1 ]</inputport>
            <inputport name="column">&quot;Rpath&quot;</inputport>
            <inputport name="valueIfNotFound">.none</inputport>
            <outputport name="result" id="v22" />
        </functor>
        <containerfunctor name="CreateString">
            <property key="dff.functor.alias" value="createString5486" />
            <inputport name="format">&quot;R CMD BATCH --no-save \&quot;--args Subset_locs=&lt;v1&gt;\&quot; -- 0_Ext_Locs_processors.R&quot;</inputport>
            <outputport name="result" id="v23" />
            <functor name="NumberValue">
                <property key="dff.functor.alias" value="numberValue5488" />
                <inputport name="value" peerid="v19" />
                <inputport name="valueNumber">1</inputport>
            </functor>
        </containerfunctor>
        <functor name="CreateLookupTable">
            <property key="dff.functor.alias" value="createLookupTable40977" />
            <property key="viewer.result" value="yes" />
            <inputport name="initialKey">1</inputport>
            <inputport name="finalKey">1</inputport>
            <inputport name="initialValue" peerid="v19" />
            <inputport name="finalValue" peerid="v19" />
            <inputport name="increment">1</inputport>
            <inputport name="keyName">&quot;Key&quot;</inputport>
            <inputport name="valueName">&quot;Value&quot;</inputport>
            <outputport name="result" id="v24" />
        </functor>
        <functor name="SaveLookupTable">
            <property key="dff.functor.alias" value="saveLookupTable40984" />
            <inputport name="table" peerid="v24" />
            <inputport name="filename">&quot;LULCC/TempTables/LocsSample.csv&quot;</inputport>
            <inputport name="suffixDigits">2</inputport>
            <inputport name="step">.none</inputport>
            <inputport name="workdir">.none</inputport>
        </functor>
    </containerfunctor>
</script>
